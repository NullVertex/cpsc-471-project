# CPSC 471 Database Project

## Database Technical Information

I've set up a MySQL database hosted on Microsoft Azure (extremely reliable) that we can build a front end against.  
Below is an EER diagram describing the tables and relationships in the database, as well as the connection information to access said database.  

You may also notice that the "notification" entity has been removed as it can be implemented in it's entirety on the client side and doesn't really need to be tracked by the database. I also broke up "showtimes" into their own entity so that they're easier to track and we still meet the project requirements.

If you have any questions, you can refer to the [database's DDL](Documentation/DDL.txt)

### EER Diagram
![EER Diagram](https://bitbucket.org/repo/o4gM44/images/469196039-EERD.PNG)