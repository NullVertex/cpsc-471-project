<html>
	<head>
		<title>Employee Login portal</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>Employee Login portal</h1>
				<ul class="nav">
					<li><a href="index.php">Showtimes</a></li>
					<li><a href="manager_login.php">Manager Login Portal</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<center>
				<form method="post" action="authentication.php">
					<span><p>Employee ID: </p><input type="text" name="id" value="" required></span>
					<?php
						date_default_timezone_set("America/Edmonton");
						if(isset($_GET['badid']) && $_GET['badid'] == 1)
						{
							echo "<p class=\"errortext\">Error: Invalid User ID</p>\n";
						}
						else
						{
							echo "<br>";
						}
					?>
					<input type="submit" name="submit" id="" Value="Login">
				</form>
			</center>
			</div>
		</section>
	</body>
</html>
