<html>
	<head>
		<title>Cineplex Movie Showtimes</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>Daily Cineplex Showtimes</h1>
				<ul class="nav">
					<li><a href="employee_login.php">Employee Login</a></li>
					<li><a href="manager_login.php">Manager Login</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<?php
					date_default_timezone_set("America/Edmonton");
					session_start();

					$hostname = "us-cdbr-azure-west-c.cloudapp.net";
					$username = "bc78a669c8608c";
					$password = "1d0ace81";
					$schema = "cpsc471";

					$con = mysqli_connect($hostname, $username, $password, $schema);

					if (mysqli_connect_errno())
					{
						echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
					}

					$query = "SELECT * FROM cpsc471.theater";

					if(($result = mysqli_query($con, $query, MYSQLI_STORE_RESULT)) && mysqli_affected_rows($con) > 0)
					{
						while($row = mysqli_fetch_assoc($result))
						{
							echo "<h1 style=\"text-align: center;\">" . $row['theater_address'] . "</h1>\n";
							echo "<p style=\"text-align: center;\">Phone number: " . $row['theater_phone_number'] . "</p>\n";

							$tid = $row['theater_id'];
							$today = date("Y-m-d");

							$mid_query = "SELECT movie_name, movie_id, movie_content_rating, movie_duration " .
							"FROM cpsc471.showtime, cpsc471.movie " .
							"WHERE showtime_date='" . $today . "'" .
							"AND showtime_movie=movie_id " .
							"AND showtime_theater=" . $tid . " " .
							"GROUP BY movie_name " .
							"ORDER BY movie_year DESC, movie_name";

							if(($mid_result = mysqli_query($con, $mid_query, MYSQLI_STORE_RESULT)) && mysqli_affected_rows($con) > 0)
							{
								while($mid_row = mysqli_fetch_assoc($mid_result))
								{
									echo "<h2>" . $mid_row['movie_name'] . "</h2>\n";
									echo "<p>" . $mid_row['movie_content_rating'] . ", " . $mid_row['movie_duration'] . " minutes</p>\n";

									$inner_query = "SELECT showtime_start FROM cpsc471.showtime " .
									"WHERE showtime_date='" . $today . "' " .
									"AND showtime_movie=" . $mid_row['movie_id'] . " " .
									"AND showtime_theater=" . $tid . " " .
									"ORDER BY showtime_start ASC";

									if($inner_result = mysqli_query($con, $inner_query))
									{
										echo "<p>\n";
										$first=1;
										while($inner_row = mysqli_fetch_assoc($inner_result))
										{
											if($first == 1)
											{
												echo date("g:i a", strtotime($inner_row['showtime_start']));
												$first=0;
											}
											else
											{
												echo ", " . date("g:i a", strtotime($inner_row['showtime_start']));
											}
										}
										echo "</p>\n";
									}
								}
							}
							echo "<hr>\n";
						}
					}
					$result->free();
				 ?>
			</div>
		</section>
	</body>
</html>
