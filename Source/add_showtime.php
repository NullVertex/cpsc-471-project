<?php
	date_default_timezone_set("America/Edmonton");
	session_start();

	$hostname = "us-cdbr-azure-west-c.cloudapp.net";
	$username = "bc78a669c8608c";
	$password = "1d0ace81";
	$schema = "cpsc471";

	$con = mysqli_connect($hostname, $username, $password, $schema);

	if (mysqli_connect_errno())
	{
		echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
	}

	//mysqli_autocommit($con, FALSE);

	if(isset($_SESSION['theater']) && isset($_POST['movie']) && isset($_POST['auditorium']) && isset($_POST['date']) && isset($_POST['time']))
	{
		$movie = $_POST['movie'];
		$date = mysqli_real_escape_string($con, $_POST['date']);
		$time = mysqli_real_escape_string($con, $_POST['time']);
		$auditorium = $_POST['auditorium'];
		$theater = $_SESSION['theater'];
		$duration=0;

		$query = "SELECT movie_duration FROM cpsc471.movie WHERE movie_id=" . $movie;
		echo "<p>" . $query . "</p>";
		if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1)
		{
			$row = mysqli_fetch_assoc($result);
			$duration = $row['movie_duration'];
		}

		if($date != "" && $time != "" && $duration > 0)
		{
			$inner_query = "SELECT showtime_start, movie_duration " .
			"FROM cpsc471.showtime, cpsc471.movie " .
			"WHERE showtime_date='" . $date . "' " .
			"AND showtime_movie=movie_id " .
			"AND showtime_theater=" . $theater . " " .
			"AND showtime_auditorium=" . $auditorium;

			echo "<p>" . $inner_query . "</p>\n";

			if(($inner_result = mysqli_query($con, $inner_query)) && mysqli_affected_rows($con) > 0)
			{
				echo "<p>Existing showtimes found. Checking for validity...</p>";
				$valid_addition=1;
				while($inner_row = mysqli_fetch_assoc($inner_result))
				{
					$existing_start_time = strtotime($inner_row['showtime_start']);
					$existing_diff = "+" . $inner_row['movie_duration'] . " minutes";
					$existing_end_time = strtotime($existing_diff, strtotime($inner_row['showtime_start']));

					$new_start_time = strtotime($time);
					$new_diff = "+" . $duration . " minutes";
					$new_end_time = strtotime($new_diff, strtotime($time));

					if(($existing_start_time <= $new_start_time) && ($new_start_time <= $existing_end_time))
					{
						echo "<p>New movie conflicts with an existing showtime</p>\n";
						$valid_addition=0;
					}
					else if(($new_end_time >= $existing_start_time) && ($new_end_time <= $existing_end_time))
					{
						echo "<p>New movie conflicts with an existing showtime</p>\n";
						$valid_addition=0;
					}
					else
					{
						echo "<p>This addition does not conflict with this existing showtime<p>\n";
					}
				}

				if($valid_addition == 1)
				{
					echo "<p>This Showtime does not conflict with any others!</p>";
					$query = "INSERT INTO cpsc471.showtime (showtime_movie, showtime_date, showtime_start, showtime_auditorium, showtime_theater)" .
					" VALUES (" . $movie . ", '" . $date . "', '" . $time . "', " . $auditorium . ", " . $theater . ")";

					if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1)
					{
						$showtime_id = mysqli_insert_id($con);
						$query = "SELECT * FROM cpsc471.movie WHERE movie_id=" . $movie;
						if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1)
						{
							$row = mysqli_fetch_assoc($result);
							$movie_length = $row['movie_duration'];
							$interval_length = 30;
							$number_of_checks = (($movie_length - ($movie_length % $interval_length)) / $interval_length) + 1;
							echo "<p>Creating a total of " . $number_of_checks . " checks</p>\n";

							for($i=0; $i < $number_of_checks; $i++)
							{
								$offset_time = $i * $interval_length;
								$offset_string = "+" . $offset_time . " minutes";
								$new_check_time = date("H:i", strtotime($offset_string, strtotime($time)));
								$query = "INSERT INTO cpsc471.check (check_schedule, check_showtime, check_time) VALUES (NULL, " . $showtime_id . ", '" . $new_check_time . "')";

								echo "<p>Running SQL: <br>" . $query . "</p>\n";

								if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1)
								{
									echo "<p>New check Successfully added!</p>\n";
								}
								else
								{
									echo "<p>Error: Check not added</p>";
								}
							}
						}
						header("Location: new_showtime_form.php?confirmed=1");
					}
					else
					{
						header("Location: new_showtime_form.php?confirmed=0");
					}
				}
				else
				{
					header("Location: new_showtime_form.php?confirmed=0");
				}
			}
			else
			{
				echo "<p>No existing showtimes for this day! Proceeding as usual</p>\n";
				$query = "INSERT INTO cpsc471.showtime (showtime_movie, showtime_date, showtime_start, showtime_auditorium, showtime_theater)" .
				" VALUES (" . $movie . ", '" . $date . "', '" . $time . "', " . $auditorium . ", " . $theater . ")";

				echo "<p>Running query: <br>" . $query . "</p>";

				if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1)
				{
					echo "<p>Successfully added new showtime</p>";

					$showtime_id = mysqli_insert_id($con);
					$query = "SELECT * FROM cpsc471.movie WHERE movie_id=" . $movie;
					if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1)
					{
						$row = mysqli_fetch_assoc($result);
						$movie_length = $row['movie_duration'];
						$interval_length = 30;
						$number_of_checks = (($movie_length - ($movie_length % $interval_length)) / $interval_length) + 1;
						echo "<p>Creating a total of " . $number_of_checks . " checks</p>\n";

						for($i=0; $i < $number_of_checks; $i++)
						{
							$offset_time = $i * $interval_length;
							$offset_string = "+" . $offset_time . " minutes";
							$new_check_time = date("H:i", strtotime($offset_string, strtotime($time)));
							$query = "INSERT INTO cpsc471.check (check_schedule, check_showtime, check_time) VALUES (NULL, " . $showtime_id . ", '" . $new_check_time . "')";

							echo "<p>Running SQL: <br>" . $query . "</p>\n";

							if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1)
							{
								echo "<p>New check Successfully added!</p>\n";
							}
							else
							{
								echo "<p>Error: Check not added</p>";
							}
						}
					}
					header("Location: new_showtime_form.php?confirmed=1");
				}
				else
				{
					header("Location: new_showtime_form.php?confirmed=0");
				}
			}
		}
	}
 ?>
