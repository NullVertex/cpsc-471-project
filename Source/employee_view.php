<html>
	<head>
		<title>Employee Home</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>Employee Home</h1>
				<ul class="nav">
					<li><a href="employee_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<center>
					<?php
						date_default_timezone_set("America/Edmonton");
						session_start();

						$db_hostname = "us-cdbr-azure-west-c.cloudapp.net";
						$db_username = "bc78a669c8608c";
						$db_password = "1d0ace81";
						$db_schema = "cpsc471";

						$con = mysqli_connect($db_hostname, $db_username, $db_password, $db_schema);

						if (mysqli_connect_errno())
						{
							echo "<p class=\"errortext\">Failed to connect to MySQL: " . mysqli_connect_error() . "</p>\n";
						}

						if(isset($_SESSION['username']) && isset($_SESSION['first_name']))
						{
							echo "<h1>Welcome, " . $_SESSION['first_name'] . "</h1><br>\n";
							$today = date("Y-m-d");

							echo "<a href=\"employee_checks.php\">View Today's checks</a><br>\n";
							echo "<a href=\"employee_weekly_schedule.php\">View Weekly Schedule</a><br>\n";
							echo "<a href=\"employee_logout.php\">Logout</a><br>\n";
						}
						else
						{
							echo "<p>No employee provided for schedule request</p>";
						}
					 ?>
				</center>
			</div>
		</section>
	</body>
</html>
