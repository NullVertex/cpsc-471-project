<html>
	<head>
		<title>Change Password</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>Change Password</h1>
				<ul class="nav">
					<li><a href="manager_view.php">Control Panel</a></li>
					<li><a href="manager_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<center>
					<?php
						date_default_timezone_set("America/Edmonton");
						session_start();

						$hostname = "us-cdbr-azure-west-c.cloudapp.net";
						$username = "bc78a669c8608c";
						$password = "1d0ace81";
						$schema = "cpsc471";

						$con = mysqli_connect($hostname, $username, $password, $schema);

						if (mysqli_connect_errno())
						{
							echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
						}

						if(isset($_GET['confirmed']))
						{
							if($_GET['confirmed'] == 1)
							{
								echo "<p>Password successfully changed</p><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
							else
							{
								echo "<p>Password change failed</p><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
						}
						else if(isset($_SESSION['username']) && isset($_POST['pw_old']) && isset($_POST['pw_new']) && isset($_POST['pw_conf']))
						{
							if($_POST['pw_new'] == $_POST['pw_conf'])
							{
								$user_id = $_SESSION['username'];
								$old_password = mysqli_real_escape_string($con, $_POST['pw_old']);
								$new_password = mysqli_real_escape_string($con, $_POST['pw_new']);

								$query = "UPDATE cpsc471.manager SET manager_pw='" . $new_password . "' WHERE manager_employee_id=" . $user_id . " AND manager_pw='" . $old_password . "'";

								//echo "<p>Executing query: <br>" . $query . "</p>\n";

								if($result = mysqli_query($con, $query) && mysqli_affected_rows($con) == 1)
								{
									if(mysqli_commit($con))
									{
										header("Location: change_pw.php?confirmed=1");
									}
									else
									{
										header("Location: change_pw.php?confirmed=0");
									}
								}
								else
								{
									header("Location: change_pw.php?confirmed=0");
								}
							}
						}
						else
						{
							echo "<form method=\"post\" action=\"change_pw.php\">\n";
								echo "<span><p>Current Password: </p><input type=\"password\" name=\"pw_old\" value=\"\" required></span>";
								echo "<span><p>New Password: </p><input type=\"password\" name=\"pw_new\" value=\"\" required></span>";
								echo "<span><p>Re-type new Password: </p><input type=\"password\" name=\"pw_conf\" value=\"\" required></span>";
								echo "<br>\n";
								echo "<input type=\"submit\" name=\"submit\" id=\"\" Value=\"Submit\">";
							echo "</form>\n";

						}
					?>
			</center>
			</div>
		</section>
	</body>
</html>
