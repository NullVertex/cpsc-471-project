<html>
	<head>
		<title>Manager Administration panel</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>Manager Control Panel</h1>
				<ul class="nav">
					<li><a href="manager_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<?php
					date_default_timezone_set("America/Edmonton");
					session_start();

					$db_hostname = "us-cdbr-azure-west-c.cloudapp.net";
					$db_username = "bc78a669c8608c";
					$db_password = "1d0ace81";
					$db_schema = "cpsc471";

					$con = mysqli_connect($db_hostname, $db_username, $db_password, $db_schema);

					if (mysqli_connect_errno())
					{
						echo "<p class=\"errortext\">Failed to connect to MySQL: " . mysqli_connect_error() . "</p>\n";
					}

					if(isset($_SESSION['manager']) && $_SESSION['manager'] == 1)
					{
						echo "<center>\n";
						if(isset($_SESSION['first_name']) && isset($_SESSION['last_name']))
						{
							echo "<h1>Welcome, " . $_SESSION['first_name'] . " " . $_SESSION['last_name'] . "</h1><br>\n";
						}

							echo "<a href=\"new_employee_form.php\">Add a new employee</a><br>\n";
							echo "<a href=\"center_employees.php\">View Employee Directory</a><br>\n";
							echo "<a href=\"manager_weekly_schedule.php\">View Weekly Schedule</a><br>\n";
							echo "<a href=\"new_schedule_form.php\">Create Schedules</a><br>\n";
							echo "<a href=\"new_showtime_form.php\">Create Showtimes</a><br>\n";
							echo "<a href=\"assign_checks.php\">Assign Today's Checks</a><br>\n";
							echo "<a href=\"new_movie_form.php\">Add New Movie</a><br>\n";
							echo "<a href=\"change_pw.php\">Change my password</a><br>\n";
							echo "<a href=\"diagnostics.php\">Diagnostic information</a><br>\n";
							echo "<a href=\"manager_logout.php\">Logout</a><br>\n";
						echo "</center>\n";
					}
					else
					{
						echo "<p>You do not have permission to view this page.</p>\n";
						echo "<a href=\"manager_login.php\">Back to login</a>\n";
					}

				 ?>
			</div>
		</section>
	</body>
</html>
