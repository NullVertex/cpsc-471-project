<html>
	<head>
		<title>New Employee form</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>New Employee form</h1>
				<ul class="nav">
					<li><a href="manager_view.php">Control Panel</a></li>
					<li><a href="manager_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<center>
					<?php
						date_default_timezone_set("America/Edmonton");
						if(isset($_GET['confirmed']))
						{
							if($_GET['confirmed'] == 1)
							{
								echo "<p>Successfully added new employee</p><br>\n";
								echo "<a href=\"new_employee_form.php\">Add another employee</a><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
							else
							{
								echo "<p class=\"errortext\">Error: Failed to add new employee</p><br>\n";
								echo "<a href=\"new_employee_form.php\">Add another employee</a><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
						}
						else
						{
							echo "<form method=\"post\" action=\"add_employee.php\">\n";
								echo "<span><p>Employee First Name: </p><input type=\"text\" name=\"f_name\" value=\"\" required></span>\n";
								echo "<span><p>Employee Middle Name: </p><input type=\"text\" name=\"m_name\" value=\"\"></span>\n";
								echo "<span><p>Employee Last Name: </p><input type=\"text\" name=\"l_name\" value=\"\" required></span>\n";
								echo "<span><p>Phone Number: </p><input type=\"text\" name=\"p_num\" value=\"(___) ___-____\" required></span>\n";
								echo "<span><p>Hire Date: </p><input type=\"text\" name=\"date\" value=\"" . Date("Y-m-d") . "\"></span>\n";
								echo "<br>\n";
								echo "<input type=\"submit\" name=\"submit\" id=\"\" Value=\"Submit\">\n";
							echo "</form>\n";
						}
					 ?>
			</center>
			</div>
		</section>
	</body>
</html>
