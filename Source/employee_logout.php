<?php
	date_default_timezone_set("America/Edmonton");
	session_start();
	ob_start();
	session_unset();
	session_destroy();
	header("location: employee_login.php");
 ?>
