<html>
	<head>
		<title>Diagnostic Information</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>Diagnostic information</h1>
				<ul class="nav">
					<li><a href="manager_view.php">Control Panel</a></li>
					<li><a href="manager_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<?php
					date_default_timezone_set("America/Edmonton");
					session_start();

					$hostname = "us-cdbr-azure-west-c.cloudapp.net";
					$username = "bc78a669c8608c";
					$password = "1d0ace81";
					$schema = "cpsc471";

					$dump_tables = 1; // Boolean. 1 to turn on, 0 to turn off

					$con = mysqli_connect($hostname, $username, $password, $schema);

					if (mysqli_connect_errno())
					{
						echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
					}
					else if($dump_tables == 1 && isset($_SESSION['manager']) && $_SESSION['manager'] == 1)
					{
						echo "<h1 style=\"text-align: center;\">Current System Time:</h1>\n";
						echo "<p style=\"text-align: center;\">" . Date("Y-m-d H:i:s") . "</p>\n";

						// Employee Table Dump
						echo "<h1 style=\"text-align: center;\">Employee Table dump</h1>";
						if($query = mysqli_query($con, "SELECT * FROM cpsc471.employee"))
						{
							echo "<table style=\"text-align: center; width: 100%;\">";
								echo "<tr>";
									echo "<th>Employee #</th>";
									echo "<th>Theater #</th>";
									echo "<th>First Name</th>";
									echo "<th>Middle Name</th>";
									echo "<th>Last Name</th>";
									echo "<th>Phone #</th>";
									echo "<th>Hire Date</th>";
								echo "</tr>";
							while($row = mysqli_fetch_assoc($query))
							{
								echo "<tr>";
									echo "<td>" . $row["employee_id"] . "</td>";
									echo "<td>" . $row["employee_tid"] . "</td>";
									echo "<td>" . $row["employee_first_name"] . "</td>";
									echo "<td>" . $row["employee_middle_name"] . "</td>";
									echo "<td>" . $row["employee_last_name"] . "</td>";
									echo "<td>" . $row["employee_phone_number"] . "</td>";
									echo "<td>" . $row["employee_hire_date"] . "</td>";
								echo "</tr>";
							}
							echo "</table>";
						}

						// Manager Table Dump
						echo "<h1 style=\"text-align: center;\">Manager Table dump</h1>";
						if($query = mysqli_query($con, "SELECT * FROM cpsc471.employee e, cpsc471.manager m WHERE e.employee_id=m.manager_employee_id"))
						{
							echo "<table style=\"text-align: center; width: 100%;\">";
								echo "<tr>";
									echo "<th>Employee #</th>";
									echo "<th>Manages theater # (From employee)</th>";
									echo "<th>First Name (From employee)</th>";
									echo "<th>Last Name (From employee)</th>";
									echo "<th>Password</th>";
								echo "</tr>";
							while($row = mysqli_fetch_assoc($query))
							{
								echo "<tr>";
									echo "<td>" . $row["manager_employee_id"] . "</td>";
									echo "<td>" . $row["employee_tid"] . "</td>";
									echo "<td>" . $row["employee_first_name"] . "</td>";
									echo "<td>" . $row["employee_last_name"] . "</td>";
									echo "<td>" . $row["manager_pw"] . "</td>";
								echo "</tr>";
							}
							echo "</table>";
						}

						// Theater Table Dump
						echo "<h1 style=\"text-align: center;\">Theater Table dump</h1>";
						if($query = mysqli_query($con, "SELECT * FROM cpsc471.theater"))
						{
							echo "<table style=\"text-align: center; width: 100%;\">";
								echo "<tr>";
									echo "<th>Theater #</th>";
									echo "<th>Address</th>";
									echo "<th>Phone #</th>";
								echo "</tr>";
							while($row = mysqli_fetch_assoc($query))
							{
								echo "<tr>";
									echo "<td>" . $row["theater_id"] . "</td>";
									echo "<td>" . $row["theater_address"] . "</td>";
									echo "<td>" . $row["theater_phone_number"] . "</td>";
								echo "</tr>";
							}
							echo "</table>";
						}

						// Auditorium Table Dump
						echo "<h1 style=\"text-align: center;\">Auditorium Table dump</h1>";
						if($query = mysqli_query($con, "SELECT * FROM cpsc471.auditorium ORDER BY auditorium_tid ASC"))
						{
							echo "<table style=\"text-align: center; width: 100%;\">";
								echo "<tr>";
									echo "<th>Theater #</th>";
									echo "<th>Auditorium #</th>";
									echo "<th>Auditorium name</th>";
									echo "<th>Experience type</th>";
								echo "</tr>";
							while($row = mysqli_fetch_assoc($query))
							{
								echo "<tr>";
									echo "<td>" . $row["auditorium_tid"] . "</td>";
									echo "<td>" . $row["auditorium_id"] . "</td>";
									echo "<td>" . $row["auditorium_name"] . "</td>";
									echo "<td>" . $row["auditorium_experience_type"] . "</td>";
								echo "</tr>";
							}
							echo "</table>";
						}

						// Movie Table Dump
						echo "<h1 style=\"text-align: center;\">Movie Table dump</h1>";
						if($query = mysqli_query($con, "SELECT * FROM cpsc471.movie"))
						{
							echo "<table style=\"text-align: center; width: 100%;\">";
								echo "<tr>";
									echo "<th>Movie ID#</th>";
									echo "<th>Title</th>";
									echo "<th>Year</th>";
									echo "<th>Content Rating</th>";
									echo "<th>Length (minutes)</th>";
								echo "</tr>";
							while($row = mysqli_fetch_assoc($query))
							{
								echo "<tr>";
									echo "<td>" . $row["movie_id"] . "</td>";
									echo "<td>" . $row["movie_name"] . "</td>";
									echo "<td>" . $row["movie_year"] . "</td>";
									echo "<td>" . $row["movie_content_rating"] . "</td>";
									echo "<td>" . $row["movie_duration"] . "</td>";
								echo "</tr>";
							}
							echo "</table>";
						}

						// Advertisement Table Dump
						echo "<h1 style=\"text-align: center;\">Advertisement Table dump</h1>";
						if($query = mysqli_query($con, "SELECT * FROM cpsc471.advertisement"))
						{
							echo "<table style=\"text-align: center; width: 100%;\">";
								echo "<tr>";
									echo "<th>ID#</th>";
									echo "<th>Promoter Movie ID#</th>";
									echo "<th>Promotee Movie ID#</th>";
								echo "</tr>";
							while($row = mysqli_fetch_assoc($query))
							{
								echo "<tr>";
									echo "<td>" . $row["advertisement_id"] . "</td>";
									echo "<td>" . $row["advertisement_promoter"] . "</td>";
									echo "<td>" . $row["advertisement_promotee"] . "</td>";
								echo "</tr>";
							}
							echo "</table>";
						}

						// Showtime Table Dump
						echo "<h1 style=\"text-align: center;\">Showtime Table dump</h1>";
						if($query = mysqli_query($con, "SELECT * FROM cpsc471.showtime ORDER BY showtime_theater ASC, showtime_auditorium ASC, showtime_id ASC"))
						{
							echo "<table style=\"text-align: center; width: 100%;\">";
								echo "<tr>";
									echo "<th>ID#</th>";
									echo "<th>Movie ID#</th>";
									echo "<th>Date</th>";
									echo "<th>Start time</th>";
									echo "<th>Auditorium #</th>";
									echo "<th>Theater #</th>";
								echo "</tr>";
							while($row = mysqli_fetch_assoc($query))
							{
								echo "<tr>";
									echo "<td>" . $row["showtime_id"] . "</td>";
									echo "<td>" . $row["showtime_movie"] . "</td>";
									echo "<td>" . $row["showtime_date"] . "</td>";
									echo "<td>" . $row["showtime_start"] . "</td>";
									echo "<td>" . $row["showtime_auditorium"] . "</td>";
									echo "<td>" . $row["showtime_theater"] . "</td>";
								echo "</tr>";
							}
							echo "</table>";
						}

						// Schedule Table Dump
						echo "<h1 style=\"text-align: center;\">Schedule Table dump</h1>";
						if($query = mysqli_query($con, "SELECT * FROM cpsc471.schedule"))
						{
							echo "<table style=\"text-align: center; width: 100%;\">";
								echo "<tr>";
									echo "<th>ID#</th>";
									echo "<th>Employee #</th>";
									echo "<th>Date</th>";
									echo "<th>Start time</th>";
									echo "<th>End time</th>";
									echo "<th>Creator Employee #</th>";
								echo "</tr>";
							while($row = mysqli_fetch_assoc($query))
							{
								echo "<tr>";
									echo "<td>" . $row["schedule_id"] . "</td>";
									echo "<td>" . $row["schedule_employee"] . "</td>";
									echo "<td>" . $row["schedule_date"] . "</td>";
									echo "<td>" . $row["schedule_start_time"] . "</td>";
									echo "<td>" . $row["schedule_end_time"] . "</td>";
									echo "<td>" . $row["schedule_creator"] . "</td>";
								echo "</tr>";
							}
							echo "</table>";
						}

						// Check Table Dump
						echo "<h1 style=\"text-align: center;\">Check Table dump</h1>";
						if($query = mysqli_query($con, "SELECT * FROM cpsc471.check"))
						{
							echo "<table style=\"text-align: center; width: 100%;\">";
								echo "<tr>";
									echo "<th>ID#</th>";
									echo "<th>Schedule ID#</th>";
									echo "<th>Showtime ID#</th>";
									echo "<th>Check Time</th>";
								echo "</tr>";
							while($row = mysqli_fetch_assoc($query))
							{
								echo "<tr>";
									echo "<td>" . $row["check_id"] . "</td>";
									echo "<td>" . $row["check_schedule"] . "</td>";
									echo "<td>" . $row["check_showtime"] . "</td>";
									echo "<td>" . $row["check_time"] . "</td>";
								echo "</tr>";
							}
							echo "</table>";
							echo "<br>";

							//phpinfo();
						}
					}
					else {
						echo "<p>Only a manager may view diagnostic information. Please <a href=\"manager_login.php\">login</a> first.</p>\n";
					}
				?>
			</div>
		</section>
	</body>
</html>
