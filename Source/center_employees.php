<html>
	<head>
		<title>Employee Directory</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>Employee Directory</h1>
				<ul class="nav">
					<li><a href="manager_view.php">Control Panel</a></li>
					<li><a href="manager_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<center>
					<?php
						date_default_timezone_set("America/Edmonton");
						session_start();

						$hostname = "us-cdbr-azure-west-c.cloudapp.net";
						$username = "bc78a669c8608c";
						$password = "1d0ace81";
						$schema = "cpsc471";

						$con = mysqli_connect($hostname, $username, $password, $schema);

						if (mysqli_connect_errno())
						{
							echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
						}

						if(isset($_SESSION['manager']) && $_SESSION['manager'] == 1 && isset($_SESSION['theater']))
						{
							$query = "SELECT * FROM cpsc471.theater WHERE theater_id=" .  $_SESSION['theater'];
							if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1)
							{
								$row = mysqli_fetch_assoc($result);
								echo "<h1>My Theater Information: </h1>";
								echo "<p>Center #: " . $row['theater_id'] . "<br>" . $row['theater_address'] . "<br>" . $row['theater_phone_number'] . "<br></p>\n";
							}

							$query = "SELECT * FROM cpsc471.employee WHERE employee_tid=" . $_SESSION['theater'];

							if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) > 0)
							{
								echo "<h1>Employees at this theater:</h1>\n";
								echo "<table style=\"text-align: center; width: 100%;\">\n";
									echo "<tr>\n";
										echo "<th>Employee #</th>\n";
										echo "<th>First Name</th>\n";
										echo "<th>Middle Name</th>\n";
										echo "<th>Last Name</th>\n";
										echo "<th>Phone #</th>\n";
										echo "<th>Hire Date</th>\n";
									echo "</tr>\n";
								while($row = mysqli_fetch_assoc($result))
								{
									echo "<tr>\n";
										echo "<td>" . $row["employee_id"] . "</td>\n";
										echo "<td>" . $row["employee_first_name"] . "</td>\n";
										echo "<td>" . $row["employee_middle_name"] . "</td>\n";
										echo "<td>" . $row["employee_last_name"] . "</td>\n";
										echo "<td>" . $row["employee_phone_number"] . "</td>\n";
										echo "<td>" . $row["employee_hire_date"] . "</td>\n";
									echo "</tr>\n";
								}
								echo "</table>\n";
							}
							else
							{
								echo "<p>There are no employees at this location!</p>\n";
							}
						}
					 ?>
				</center>
			</div>
		</section>
	</body>
</html>
