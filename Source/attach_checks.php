<?php
	date_default_timezone_set("America/Edmonton");
	session_start();

	$hostname = "us-cdbr-azure-west-c.cloudapp.net";
	$username = "bc78a669c8608c";
	$password = "1d0ace81";
	$schema = "cpsc471";

	$con = mysqli_connect($hostname, $username, $password, $schema);

	if (mysqli_connect_errno())
	{
		echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
	}

	if(isset($_POST['sched_id']))
	{
		if(!isset($_POST['check_id']))
		{
			header("Location: assign_checks.php?noneselected=1");
		}
		else
		{
			$schedule_id = $_POST['sched_id'];
			$num_assigned = count($_POST['check_id']);
			$values = $_POST['check_id'];

			$succeed=1;

			for($i=0; $i < $num_assigned; $i++)
			{
				$query = "UPDATE cpsc471.check SET check_schedule=" . $schedule_id . " WHERE check_id=" . $values[$i];
				//echo $query . "<br>\n";

				if(!(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1))
				{
					$succeed=0;
				}
			}

			if($succeed == 1)
			{
				header("Location: assign_checks.php?confirmed=1");
			}
			else
			{
				header("Location: assign_checks.php?confirmed=0");
			}
		}
	}
 ?>
