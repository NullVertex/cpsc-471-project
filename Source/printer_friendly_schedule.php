<html>
	<head>
		<title>Cineplex Weekly Schedule</title>
		<style>
			table, th, td ,tr {
				font: 14px/1.8em 'Source Sans Pro', Helvetica, Arial, Helvetica, sans-serif;
				text-align: center;
				border-collapse: collapse;
				border: 1px solid black;
			}

			th {
				font-weight: bold;
			}

			h1 {
				font: 28px/1.8em 'Source Sans Pro', Helvetica, Arial, Helvetica, sans-serif;
			}

			a {
				font: 14px/1.8em 'Source Sans Pro', Helvetica, Arial, Helvetica, sans-serif;
			}
		</style>
	</head>
	<body>
		<center>
			<h1>Cineplex Weekly Schedule</h1>
			<?php
				date_default_timezone_set("America/Edmonton");
				session_start();

				$hostname = "us-cdbr-azure-west-c.cloudapp.net";
				$username = "bc78a669c8608c";
				$password = "1d0ace81";
				$schema = "cpsc471";

				$con = mysqli_connect($hostname, $username, $password, $schema);

				if (mysqli_connect_errno())
				{
					echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
				}

				if(isset($_SESSION['theater']))
				{
					$week_start = date("Y-m-d", strtotime("last sunday"));
					$week_end = date("Y-m-d", strtotime("this sunday"));

					$date_array = new DatePeriod(new DateTime($week_start), new DateInterval('P1D'), new DateTime($week_end));
					$day_of_week_array = [];
					echo "<table style=\"width: 100%;\">\n";
					echo "<tr>\n";
						echo "<th>Employee</th>\n";
					foreach($date_array as $date)
					{
						echo "<th>" . $date->format('l,') . "<br>" . $date->format('F jS') . "</th>\n";
						$day_of_week_array[] = $date->format("Y-m-d");
					}
					echo "</tr>\n";

					$query = "SELECT * FROM cpsc471.employee WHERE employee_tid=" . $_SESSION['theater'] . " ORDER BY employee_id ASC";

					if(($result = mysqli_query($con, $query, MYSQLI_STORE_RESULT)) && mysqli_affected_rows($con) > 0)
					{
						while($row = mysqli_fetch_assoc($result))
						{
							echo "<tr>\n";
							$employee = $row['employee_id'];
							$name = $row['employee_first_name'] . " " . $row['employee_last_name'];
							echo "<td>" . $employee . "<br>" . $name . "</td>\n";

							for($i=0; $i < 7; $i++)
							{
								$inner_query = "SELECT * FROM cpsc471.schedule, cpsc471.employee " .
								"WHERE employee_id=" . $employee . " " .
								"AND employee_id=schedule_employee " .
								"AND schedule_date='" . $day_of_week_array[$i] . "'";

								if(($inner_result = mysqli_query($con, $inner_query)) && mysqli_affected_rows($con) == 1)
								{
									$col = mysqli_fetch_assoc($inner_result);

									echo "<td>" . date("g:i A", strtotime($col['schedule_start_time'])) . "<br>to<br>" . date("g:i A", strtotime($col['schedule_end_time'])) . "</td>\n";
								}
								else
								{
									echo "<td></td>\n";
								}
							}

							echo "</tr>\n";
						}
					}
					echo "</table>\n";
				}
			 ?>
			 <script>
			    document.write('<a href="' + document.referrer + '">Go Back</a>');
			</script>
		</center>
	</body>
</html>
