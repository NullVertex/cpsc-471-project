<html>
	<head>
		<title>Weekly Schedule</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>Weekly Schedule</h1>
				<ul class="nav">
					<li><a href="employee_view.php">Home</a></li>
					<li><a href="employee_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<center>
					<?php
						date_default_timezone_set("America/Edmonton");
						session_start();

						$hostname = "us-cdbr-azure-west-c.cloudapp.net";
						$username = "bc78a669c8608c";
						$password = "1d0ace81";
						$schema = "cpsc471";

						$con = mysqli_connect($hostname, $username, $password, $schema);

						if (mysqli_connect_errno())
						{
							echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
						}

						if(isset($_SESSION['theater']))
						{
							$week_start = date("Y-m-d", strtotime("last sunday"));
							$week_end = date("Y-m-d", strtotime("this sunday"));

							$date_array = new DatePeriod(new DateTime($week_start), new DateInterval('P1D'), new DateTime($week_end));
							$day_of_week_array = [];
							echo "<table style=\"width: 100%;\">\n";
							echo "<tr>\n";
								echo "<th>Employee</th>\n";
							foreach($date_array as $date)
							{
								echo "<th>" . $date->format('l,') . "<br>" . $date->format('F jS') . "</th>\n";
								$day_of_week_array[] = $date->format("Y-m-d");
							}
							echo "</tr>\n";

							$query = "SELECT * FROM cpsc471.employee WHERE employee_tid=" . $_SESSION['theater'] . " ORDER BY employee_id ASC";

							if(($result = mysqli_query($con, $query, MYSQLI_STORE_RESULT)) && mysqli_affected_rows($con) > 0)
							{
								while($row = mysqli_fetch_assoc($result))
								{
									echo "<tr>\n";
									$employee = $row['employee_id'];
									$name = $row['employee_first_name'] . " " . $row['employee_last_name'];
									echo "<td>" . $employee . "<br>" . $name . "</td>\n";

									for($i=0; $i < 7; $i++)
									{
										$inner_query = "SELECT * FROM cpsc471.schedule, cpsc471.employee " .
										"WHERE employee_id=" . $employee . " " .
										"AND employee_id=schedule_employee " .
										"AND schedule_date='" . $day_of_week_array[$i] . "'";

										if(($inner_result = mysqli_query($con, $inner_query)) && mysqli_affected_rows($con) == 1)
										{
											$col = mysqli_fetch_assoc($inner_result);

											echo "<td>" . date("g:i A", strtotime($col['schedule_start_time'])) . "<br>to<br>" . date("g:i A", strtotime($col['schedule_end_time'])) . "</td>\n";
										}
										else
										{
											echo "<td></td>\n";
										}
									}

									echo "</tr>\n";
								}
							}
							echo "</table>\n";
						}
					 ?>
					<a href="printer_friendly_schedule.php">Printer Friendly Version</a>
				</center>
			</div>
		</section>
	</body>
</html>
