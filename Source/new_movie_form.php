<html>
	<head>
		<title>New Movie form</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>New Movie form</h1>
				<ul class="nav">
					<li><a href="manager_view.php">Control Panel</a></li>
					<li><a href="manager_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<center>
					<?php
						date_default_timezone_set("America/Edmonton");
						if(isset($_GET['confirmed']))
						{
							if($_GET['confirmed'] == 1)
							{
								echo "<p>Successfully added new movie</p><br>\n";
								echo "<a href=\"new_employee_form.php\">Add another movie</a><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
							else
							{
								echo "<p class=\"errortext\">Error: Failed to add new movie/p><br>\n";
								echo "<a href=\"new_employee_form.php\">Add another movie</a><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
						}
						else
						{
							echo "<form method=\"post\" action=\"add_movie.php\">\n";
								echo "<span><p>Title: </p><input type=\"text\" name=\"title\" value=\"\" required></span>\n";
								echo "<span><p>Year: </p><input type=\"text\" name=\"year\" value=\"" . date("Y") . "\" required></span>\n";
								echo "<span><p>Content Rating: </p><select name=\"rating\">\n";
									echo "<option value=\"G\">G</option>\n";
									echo "<option value=\"PG\">PG</option>\n";
									echo "<option value=\"PG-13\">PG-13</option>\n";
									echo "<option value=\"R\">R</option>\n";
								echo "</select></span>\n";
								echo "<span><p>Duration(Minutes): </p><input type=\"text\" name=\"duration\" value=\"\" required></span>\n";
								echo "<br>\n";
								echo "<input type=\"submit\" name=\"submit\" id=\"\" Value=\"Submit\">\n";
							echo "</form>\n";
						}
					 ?>
			</center>
			</div>
		</section>
	</body>
</html>
