<html>
	<head>
		<title>New Schedule form</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>New Schedule form</h1>
				<ul class="nav">
					<li><a href="manager_view.php">Control Panel</a></li>
					<li><a href="manager_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<center>
					<?php
						date_default_timezone_set("America/Edmonton");
						session_start();

						$hostname = "us-cdbr-azure-west-c.cloudapp.net";
						$username = "bc78a669c8608c";
						$password = "1d0ace81";
						$schema = "cpsc471";

						$con = mysqli_connect($hostname, $username, $password, $schema);

						if (mysqli_connect_errno())
						{
							echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
						}

						if(isset($_GET['alreadyscheduled']) && $_GET['alreadyscheduled'] == 1)
						{
							echo "<p class=\"errortext\">Error: This employee has already been scheduled for this date.</p>\n";
							echo "<a href=\"new_schedule_form.php\">Create another schedule</a><br>\n";
							echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
						}
						else if(isset($_GET['confirmed']))
						{
							if($_GET['confirmed'] == 1)
							{
								echo "<p>Schedule was successfully created</p>\n";
								echo "<a href=\"new_schedule_form.php\">Create another schedule</a><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
							else
							{
								echo "<p class=\"errortext\">Error: Unable to create this scheudle</p>\n";
								echo "<a href=\"new_schedule_form.php\">Create another schedule</a><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
						}
						else if(isset($_SESSION['theater']))
						{
							echo "<form method=\"post\" action=\"add_schedule.php\">\n";
								$query = "SELECT * FROM cpsc471.employee WHERE employee_tid=" . $_SESSION['theater'];
								if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) > 0)
								{
									echo "<span><p>Employee: </p><select name=\"id\">\n";
									while($row = mysqli_fetch_assoc($result))
									{
										echo "<option value=" . $row['employee_id'] . ">" . $row['employee_first_name'] . " " . $row['employee_last_name'] . "</option>\n";
									}
									echo "</select></span>\n";
									echo "<span><p>Date: </p><input type=\"text\" name=\"date\" value=\"" . date("Y-m-d") . "\"></span>\n";
									echo "<span><p>Start time: </p><input type=\"text\" name=\"start\" value=\"__:__\"></span>\n";
									echo "<span><p>End time: </p><input type=\"text\" name=\"end\" value=\"__:__\"></span>\n";
								}
								echo "<br>\n";
								echo "<input type=\"submit\" name=\"submit\" id=\"\" Value=\"Submit\">\n";
							echo "</form>\n";
						}
					 ?>
			</center>
			</div>
		</section>
	</body>
</html>
