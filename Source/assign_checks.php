<html>
	<head>
		<title>Assign Checks</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>Assign Checks</h1>
				<ul class="nav">
					<li><a href="manager_view.php">Control Panel</a></li>
					<li><a href="manager_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<center>
					<?php
						date_default_timezone_set("America/Edmonton");
						session_start();

						$hostname = "us-cdbr-azure-west-c.cloudapp.net";
						$username = "bc78a669c8608c";
						$password = "1d0ace81";
						$schema = "cpsc471";

						$con = mysqli_connect($hostname, $username, $password, $schema);

						if (mysqli_connect_errno())
						{
							echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
						}

						if(isset($_GET['noneselected']) && $_GET['noneselected'] == 1)
						{
							echo "<p class=\"errortext\">Error: No checks were selected</p>\n";
							echo "<a href=\"assign_checks.php\">Try again</a><br>\n";
							echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
						}
						else if(isset($_GET['confirmed']))
						{
							if($_GET['confirmed'] == 1)
							{
								echo "<p>Checks successfully assigned to this employee</p>\n";
								echo "<a href=\"assign_checks.php\">Assign more checks</a><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
							else
							{
								echo "<p class=\"errortext\">Error: Unable to assign these checks</p>\n";
								echo "<a href=\"assign_checks.php\">Try again</a><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
						}
						else if(isset($_POST['sched_id']))
						{
							echo "<form method=\"post\" action=\"attach_checks.php\">\n";
								echo "<span><p>Selected Schedule ID: </p><input name=\"sched_id\" value=\"" . $_POST['sched_id'] . "\"type=\"text\" readonly></span><br>\n";
								$query = "SELECT * FROM cpsc471.schedule WHERE schedule_id=" . $_POST['sched_id'];

								if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1)
								{
									$row = mysqli_fetch_assoc($result);
									$start_time = strtotime($row['schedule_start_time']);
									$end_time = strtotime($row['schedule_end_time']);

									$query = "SELECT check_id, movie_name, auditorium_name, check_time " .
									"FROM cpsc471.check, cpsc471.showtime, cpsc471.movie, cpsc471.auditorium " .
									"WHERE check_schedule IS NULL " .
									"AND check_showtime=showtime_id " .
									"AND showtime_date='" . date("Y-m-d") . "' " .
									"AND auditorium_id=showtime_auditorium " .
									"AND auditorium_id=showtime_auditorium " .
									"AND auditorium_tid=showtime_theater " .
									"AND auditorium_tid=" . $_SESSION['theater'] . " " .
									"AND showtime_movie=movie_id " .
									"ORDER BY check_time ASC";

									if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) > 0)
									{
										while($row = mysqli_fetch_assoc($result))
										{
											$check_time = strtotime($row['check_time']);

											$count=0;
											if($start_time <= $check_time && $check_time <= $end_time)
											{
												$string = date("H:i", strtotime($row['check_time'])) . ", " . $row['movie_name'] . ", " . $row['auditorium_name'];
												echo "<span><input type=\"checkbox\" name=\"check_id[]\" value=" . $row['check_id'] . ">" . $string . "</span><br>\n";
												$count++;
											}
										}
										if($count > 0)
										{
											echo "<br>\n";
											echo "<input type=\"submit\" name=\"submit\" Value=\"Assign to this employee\">\n";
										}
										else
										{
											echo "<p class=\"errortext\">Error: There are no available checks which can be assigned to this employee</p>\n";
											echo "<a href=\"assign_checks.php\">Select a different employee</a><br>\n";
											echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
										}
									}
									else
									{
										echo "<p class=\"errortext\">Error: There are no available checks which can be assigned to this employee</p>\n";
										echo "<a href=\"assign_checks.php\">Select a different employee</a><br>\n";
										echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
									}
								}

							echo "</form>\n";
						}
						else if(isset($_SESSION['theater']))
						{
							echo "<form method=\"post\" action=\"assign_checks.php\">\n";

								$query = "SELECT * FROM cpsc471.employee, cpsc471.schedule WHERE employee_tid=" . $_SESSION['theater'] .
								" AND schedule_employee=employee_id AND schedule_date='" . date("Y-m-d") . "'";
								//echo "<p>Running query: <br>" . $query . "</p>\n";

								if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) > 0)
								{
									echo "<span><p>Employees Scheduled Today: </p><select name=\"sched_id\">\n";
									while($row = mysqli_fetch_assoc($result))
									{
										$string = $row['employee_first_name'] . " " . $row['employee_last_name'] . ", " . date("H:i", strtotime($row['schedule_start_time'])) . "-" . date("H:i", strtotime($row['schedule_end_time']));
										echo "<option value=" . $row['schedule_id'] . ">" . $string . "</option>\n";
									}
									echo "</select></span>\n";
									echo "<br>\n";
									echo "<input type=\"submit\" name=\"submit\" Value=\"Submit\">\n";
								}
								else
								{
									echo "<p class=\"errortext\">Error: No Employees scheduled today!</p>\n";
								}

							echo "</form>\n";
						}
					 ?>
				</center>
			</div>
		</section>
	</body>
</html>
