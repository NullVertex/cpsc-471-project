<html>
	<head>
		<title>Schedule</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>My Schedule</h1>
				<ul class="nav">
					<li><a href="employee_view.php">Home</a></li>
					<li><a href="employee_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<?php
					date_default_timezone_set("America/Edmonton");
					session_start();

					$db_hostname = "us-cdbr-azure-west-c.cloudapp.net";
					$db_username = "bc78a669c8608c";
					$db_password = "1d0ace81";
					$db_schema = "cpsc471";

					$con = mysqli_connect($db_hostname, $db_username, $db_password, $db_schema);

					if (mysqli_connect_errno())
					{
						echo "<p class=\"errortext\">Failed to connect to MySQL: " . mysqli_connect_error() . "</p>\n";
					}

					if(isset($_SESSION['username']) && isset($_SESSION['first_name']))
					{
						//echo "<p>Welcome, " . $_SESSION['first_name'] . "</p>\n";
						$today = date("Y-m-d");

						$query = 	"SELECT schedule_start_time, schedule_end_time, movie_name, auditorium_name, check_time" .
									" FROM cpsc471.schedule, cpsc471.check, cpsc471.showtime, cpsc471.auditorium, cpsc471.movie" .
									" WHERE schedule_employee=" . $_SESSION['username'] .
									" AND schedule_date='" . $today . "'" .
									" AND check_schedule=schedule_id" .
									" AND check_showtime=showtime_id" .
									" AND showtime_movie=movie_id" .
									" AND showtime_auditorium=auditorium_id" .
									" AND showtime_theater=auditorium_tid" .
									" ORDER BY check_time ASC";
						//echo "<p> Executing query: <br>" . $query . "</p>\n";

						if($result = mysqli_query($con, $query, MYSQLI_STORE_RESULT))
						{
							if(mysqli_affected_rows($con) > 0)
							{
								echo "<p>Today's schedule is as follows: </p>\n";
								echo "<table style=\"width: 100%;\">";
								echo "<tr>\n";
									echo "<th>Time</th>\n";
									echo "<th>Movie</th>\n";
									echo "<th>Auditorium</th>\n";
								echo "</tr>\n";
								while($row = mysqli_fetch_assoc($result))
								{
									echo "<tr>\n";
										echo "<td>" . date("g:i a", strtotime($row['check_time'])) . "</td>\n";
										echo "<td>" . $row['movie_name'] . "</td>\n";
										echo "<td>" . $row['auditorium_name'] . "</td>\n";
									echo "</tr>\n";

									$popup_text = "\"Please go check " . $row['auditorium_name'] . "\"";
									$time_diff = (strtotime($row['check_time']) - time()) * 1000;

									if($time_diff > 0)
									{
										echo "<script>\n";
										echo "setTimeout(function(){alert(" . $popup_text . ");}, " . $time_diff . ");\n";
										echo "</script>\n";
									}
								}
								echo "</table>";
							}
							else
							{
								echo "<p>You have not been assigned any checks for today.</p>\n";
							}
						}
					}
					else
					{
						echo "<p>No employee provided for schedule request</p>";
					}
				 ?>
			</div>
		</section>
	</body>
</html>
