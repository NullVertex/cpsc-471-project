<?php
	date_default_timezone_set("America/Edmonton");
	session_start();

	$hostname = "us-cdbr-azure-west-c.cloudapp.net";
	$username = "bc78a669c8608c";
	$password = "1d0ace81";
	$schema = "cpsc471";

	$con = mysqli_connect($hostname, $username, $password, $schema);

	if (mysqli_connect_errno())
	{
		echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
	}

	if(isset($_SESSION['theater']) && isset($_POST['f_name']) && isset($_POST['m_name']) && isset($_POST['l_name']) && isset($_POST['p_num']) && isset($_POST['date']))
	{
		$first_name = "'" . mysqli_real_escape_string($con, $_POST['f_name']) . "'";
		$middle_name = "'" . mysqli_real_escape_string($con, $_POST['m_name']) . "'";
		$last_name = "'" . mysqli_real_escape_string($con, $_POST['l_name']) . "'";
		$phone_number = "'" . mysqli_real_escape_string($con, $_POST['p_num']) . "'";
		$hire_date = "'" . mysqli_real_escape_string($con, $_POST['date']) . "'";
		$theater_id = $_SESSION['theater'];

		if($middle_name == "''")
		{
			$middle_name = "NULL";
		}

		if($first_name != "" && $last_name != "" && $phone_number != "" && $hire_date != "")
		{
			$query = "INSERT INTO cpsc471.employee " .
			"(employee_first_name, employee_middle_name, employee_last_name, employee_phone_number, employee_hire_date, employee_tid) " .
			"VALUES(" . $first_name . ", " . $middle_name . ", " . $last_name . ", " . $phone_number . ", " . $hire_date . ", " . $theater_id . ")";

			//echo "<p>Running query: <br>" . $query . "</p>\n";

			if($result = mysqli_query($con, $query) && mysqli_affected_rows($con) == 1)
			{
				header("Location: new_employee_form.php?confirmed=1");
			}
			else
			{
				header("Location: new_employee_form.php?confirmed=0");
			}
		}
	}
 ?>
