<?php
	date_default_timezone_set("America/Edmonton");
	session_start();

	$db_hostname = "us-cdbr-azure-west-c.cloudapp.net";
	$db_username = "bc78a669c8608c";
	$db_password = "1d0ace81";
	$db_schema = "cpsc471";

	$con = mysqli_connect($db_hostname, $db_username, $db_password, $db_schema);

	if (mysqli_connect_errno())
	{
		echo "<head>\n";
			echo "<title>Authentication page</title>\n";
			echo "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />\n";
		echo "</head>\n";
		echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
	}

	$_SESSION['username'] = "";
	$_SESSION['password'] = "";
	$_SESSION['theater'] = "";
	$_SESSION['first_name'] = "";
	$_SESSION['middle_name'] = "";
	$_SESSION['last_name'] = "";
	$_SESSION['phone_number'] = "";

	if(isset($_POST['id']))
	{
		$_SESSION['username'] = mysqli_real_escape_string($con, $_POST['id']);
	}


	$query = "SELECT * FROM cpsc471.employee WHERE employee_id=" . $_SESSION['username'];
	if($query_employee = mysqli_query($con, $query, MYSQLI_STORE_RESULT))
	{
		if(mysqli_affected_rows($con) == 1)
		{
			if($row = mysqli_fetch_assoc($query_employee))
			{
				$_SESSION['theater'] = $row['employee_tid'];
				$_SESSION['first_name'] = $row['employee_first_name'];
				$_SESSION['middle_name'] = $row['employee_middle_name'];
				$_SESSION['last_name'] = $row['employee_last_name'];
				$_SESSION['phone_number'] = $row['employee_phone_number'];

				if(isset($_POST['pw']))
				{
					$_SESSION['password'] = mysqli_real_escape_string($con, $_POST['pw']);

					$query = "SELECT * FROM cpsc471.manager WHERE manager_employee_id=" . $_SESSION['username'];
					if($query_managers = mysqli_query($con, $query, MYSQLI_STORE_RESULT))
					{
						if($row = mysqli_fetch_assoc($query_managers))
						{
							if($row['manager_pw'] == $_SESSION['password'])
							{
								// Redirect to manager view page
								$_SESSION['manager'] = 1;
								header('Location: manager_view.php');
								die();
							}
							else
							{
								header('Location: manager_login.php?badpw=1');
								die();
							}
						}
					}
				}
				else
				{
					// Redirect to employee view page
					header('Location: employee_view.php');
					die();
				}
			}
		}
		else
		{
			//echo "<p>The specified employee ID was not found</p>\n";
			header('Location: employee_login.php?badid=1');
		}

	}
	$query_employee->free();
	$query_managers->free();

?>
