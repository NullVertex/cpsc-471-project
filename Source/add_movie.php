<?php
	date_default_timezone_set("America/Edmonton");
	session_start();

	$hostname = "us-cdbr-azure-west-c.cloudapp.net";
	$username = "bc78a669c8608c";
	$password = "1d0ace81";
	$schema = "cpsc471";

	$con = mysqli_connect($hostname, $username, $password, $schema);

	if (mysqli_connect_errno())
	{
		echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
	}

	if(isset($_POST['title']) && isset($_POST['year']) && isset($_POST['rating']) && isset($_POST['duration']))
	{
		$title = mysqli_real_escape_string($con, $_POST['title']);
		$year = mysqli_real_escape_string($con, $_POST['year']);
		$rating = $_POST['rating'];
		$duration = mysqli_real_escape_string($con, $_POST['duration']);
		$query = "INSERT INTO cpsc471.movie (movie_name, movie_year, movie_content_rating, movie_duration) " .
		"VALUES('" . $title . "', '" . $year . "', '" . $rating . "', " . $duration . ")";

		//echo $query;

		if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) == 1)
		{
			header("Location: new_movie_form.php?confirmed=1");
		}
		else
		{
			header("Location: new_movie_form.php?confirmed=0");
		}
	}
	else
	{
		header("Location: new_movie_form.php?confirmed=0");
	}
 ?>
