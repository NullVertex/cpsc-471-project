<html>
	<head>
		<title>New Showtime form</title>
		<link rel="stylesheet" href="style.css" type="text/css" />
	</head>
	<body>
		<header id="header">
			<div class="inner clearfix">
				<h1>New Showtime form</h1>
				<ul class="nav">
					<li><a href="manager_view.php">Control Panel</a></li>
					<li><a href="manager_logout.php">Logout</a></li>
				</ul>
			</div>
		</header>
		<section id="content">
			<div class="inner">
				<center>
					<?php
						date_default_timezone_set("America/Edmonton");
						session_start();

						$hostname = "us-cdbr-azure-west-c.cloudapp.net";
						$username = "bc78a669c8608c";
						$password = "1d0ace81";
						$schema = "cpsc471";

						$con = mysqli_connect($hostname, $username, $password, $schema);

						if (mysqli_connect_errno())
						{
							echo "<p class=\"errortext\">Failed to connect to MySQL: <br>" . mysqli_connect_error() . "</p>\n";
						}
						if(isset($_GET['confirmed']))
						{
							if($_GET['confirmed'] == 1)
							{
								echo "<p>Successfully added new showtime</p><br>\n";
								echo "<a href=\"new_showtime_form.php\">Add another showtime</a><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
							else
							{
								echo "<p class=\"errortext\">Error: Failed to add new showtime</p><br>\n";
								echo "<a href=\"new_showtime_form.php\">Add another showtime</a><br>\n";
								echo "<a href=\"manager_view.php\">Back to Control Panel</a><br>\n";
							}
						}
						else if(isset($_SESSION['theater']) && isset($_SESSION['manager']) && $_SESSION['manager'] == 1)
						{
							echo "<form method=\"post\" action=\"add_showtime.php\">\n";

								$submit_enabled = 1;

								$query = "SELECT * FROM cpsc471.movie";
								if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) > 0)
								{
									echo "<span><p>Movie: </p><select name=\"movie\">\n";
									while($row = mysqli_fetch_assoc($result))
									{
										echo "<option value=" . $row['movie_id'] . ">" . $row['movie_name'] . "</option>\n";
									}
									echo "</select></span>\n";
								}
								else
								{
									echo "<p class=\"errortext\">Error: Could not retrieve any movies</p><br>\n";
									$submit_enabled = 0;
								}

								$query = "SELECT * FROM cpsc471.auditorium WHERE auditorium_tid=" . $_SESSION['theater'];
								if(($result = mysqli_query($con, $query)) && mysqli_affected_rows($con) > 0)
								{
									echo "<span><p>Auditorium: </p><select name=\"auditorium\">\n";
									while($row = mysqli_fetch_assoc($result))
									{
										echo "<option value=" . $row['auditorium_id'] . ">" . $row['auditorium_name'] . ", " . $row['auditorium_experience_type'] . "</option>\n";
									}
									echo "</select></span>\n";
								}
								else
								{
									echo "<p class=\"errortext\">Error: Could not retrieve any auditoriums</p><br>\n";
									$submit_enabled = 0;
								}

								echo "<span><p>Date: </p><input type=\"text\" name=\"date\" value=\"" . Date("Y-m-d") . "\"></span>\n";
								echo "<span><p>Time: </p><input type=\"text\" name=\"time\" value=\"" . Date("H:i") . "\"></span>\n";
								echo "<br>\n";
								if($submit_enabled == 1)
								{
									echo "<input type=\"submit\" name=\"submit\" id=\"\" Value=\"Submit\">\n";
								}
								else
								{
									echo "<input type=\"submit\" name=\"submit\" id=\"\" Value=\"Submit\" disabled>\n";
								}


							echo "</form>\n";
						}
					 ?>
			</center>
			</div>
		</section>
	</body>
</html>
