SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema cpsc471
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cpsc471` DEFAULT CHARACTER SET utf8 ;
USE `cpsc471` ;

-- -----------------------------------------------------
-- Table `cpsc471`.`movie`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpsc471`.`movie` (
  `movie_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `movie_name` VARCHAR(255) NOT NULL,
  `movie_year` YEAR NOT NULL,
  `movie_content_rating` VARCHAR(255) NOT NULL,
  `movie_duration` INT(10) NOT NULL,
  PRIMARY KEY (`movie_id`),
  UNIQUE INDEX `movie_id_UNIQUE` (`movie_id` ASC),
  INDEX `movie_index_UNIQUE` (`movie_name` ASC, `movie_year` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cpsc471`.`advertisement`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpsc471`.`advertisement` (
  `advertisement_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `advertisement_promoter` INT(10) UNSIGNED NOT NULL,
  `advertisement_promotee` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`advertisement_id`),
  UNIQUE INDEX `advertisement_index_UNIQUE` (`advertisement_promoter` ASC, `advertisement_promotee` ASC),
  INDEX `advertisement_foreign_promotee_idx` (`advertisement_promotee` ASC),
  CONSTRAINT `advertisement_foreign_promotee`
    FOREIGN KEY (`advertisement_promotee`)
    REFERENCES `cpsc471`.`movie` (`movie_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `advertisement_foreign_promoter`
    FOREIGN KEY (`advertisement_promoter`)
    REFERENCES `cpsc471`.`movie` (`movie_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cpsc471`.`theater`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpsc471`.`theater` (
  `theater_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `theater_address` VARCHAR(255) NOT NULL,
  `theater_phone_number` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`theater_id`),
  UNIQUE INDEX `theater_id_UNIQUE` (`theater_id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cpsc471`.`auditorium`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpsc471`.`auditorium` (
  `auditorium_id` INT(10) NOT NULL,
  `auditorium_tid` INT(10) UNSIGNED NOT NULL,
  `auditorium_name` VARCHAR(255) NOT NULL,
  `auditorium_experience_type` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`auditorium_id`, `auditorium_tid`),
  INDEX `auditorium_foreign_theater_idx` (`auditorium_tid` ASC),
  CONSTRAINT `auditorium_foreign_theater`
    FOREIGN KEY (`auditorium_tid`)
    REFERENCES `cpsc471`.`theater` (`theater_id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cpsc471`.`employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpsc471`.`employee` (
  `employee_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employee_first_name` VARCHAR(255) NOT NULL,
  `employee_middle_name` VARCHAR(255) NULL DEFAULT NULL,
  `employee_last_name` VARCHAR(255) NOT NULL,
  `employee_phone_number` VARCHAR(255) NOT NULL,
  `employee_hire_date` DATE NOT NULL,
  `employee_tid` INT(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`employee_id`),
  UNIQUE INDEX `employee_id_UNIQUE` (`employee_id` ASC),
  INDEX `employee_foreign_theater_idx` (`employee_tid` ASC),
  CONSTRAINT `employee_foreign_theater`
    FOREIGN KEY (`employee_tid`)
    REFERENCES `cpsc471`.`theater` (`theater_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cpsc471`.`manager`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpsc471`.`manager` (
  `manager_employee_id` INT(10) UNSIGNED NOT NULL,
  `manager_pw` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`manager_employee_id`),
  UNIQUE INDEX `manager_employee_id_UNIQUE` (`manager_employee_id` ASC),
  CONSTRAINT `manager_foreign_employee`
    FOREIGN KEY (`manager_employee_id`)
    REFERENCES `cpsc471`.`employee` (`employee_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cpsc471`.`schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpsc471`.`schedule` (
  `schedule_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `schedule_employee` INT(10) UNSIGNED NULL DEFAULT NULL,
  `schedule_date` DATE NOT NULL,
  `schedule_start_time` TIME NOT NULL,
  `schedule_end_time` TIME NOT NULL,
  `schedule_creator` INT(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`schedule_id`),
  UNIQUE INDEX `schedule_id_UNIQUE` (`schedule_id` ASC),
  UNIQUE INDEX `schedule_index_UNIQUE` (`schedule_employee` ASC, `schedule_date` ASC),
  INDEX `schedule_foreign_manager_idx` (`schedule_creator` ASC),
  CONSTRAINT `schedule_foreign_employee`
    FOREIGN KEY (`schedule_employee`)
    REFERENCES `cpsc471`.`employee` (`employee_id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `schedule_foreign_manager`
    FOREIGN KEY (`schedule_creator`)
    REFERENCES `cpsc471`.`manager` (`manager_employee_id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cpsc471`.`showtime`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpsc471`.`showtime` (
  `showtime_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `showtime_movie` INT(10) UNSIGNED NOT NULL,
  `showtime_date` DATE NOT NULL,
  `showtime_start` TIME NOT NULL,
  `showtime_auditorium` INT(10) NOT NULL,
  `showtime_theater` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`showtime_id`),
  UNIQUE INDEX `showtime_id_UNIQUE` (`showtime_id` ASC),
  INDEX `showtime_foreign_movie_idx` (`showtime_movie` ASC),
  INDEX `showtime_foreign_auditorium_idx` (`showtime_auditorium` ASC, `showtime_theater` ASC),
  CONSTRAINT `showtime_foreign_auditorium`
    FOREIGN KEY (`showtime_auditorium` , `showtime_theater`)
    REFERENCES `cpsc471`.`auditorium` (`auditorium_id` , `auditorium_tid`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `showtime_foreign_movie`
    FOREIGN KEY (`showtime_movie`)
    REFERENCES `cpsc471`.`movie` (`movie_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `cpsc471`.`check`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cpsc471`.`check` (
  `check_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `check_schedule` INT(10) UNSIGNED,
  `check_showtime` INT(10) UNSIGNED NOT NULL,
  `check_time` TIME NOT NULL,
  PRIMARY KEY (`check_id`),
  UNIQUE INDEX `check_id_UNIQUE` (`check_id` ASC),
  INDEX `check_foreign_schedule_idx` (`check_schedule` ASC),
  INDEX `check_foreign_showtime_idx` (`check_showtime` ASC),
  CONSTRAINT `check_foreign_schedule`
    FOREIGN KEY (`check_schedule`)
    REFERENCES `cpsc471`.`schedule` (`schedule_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `check_foreign_showtime`
    FOREIGN KEY (`check_showtime`)
    REFERENCES `cpsc471`.`showtime` (`showtime_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
